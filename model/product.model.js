const mongoose = require('mongoose');

const productSchema= new mongoose.Schema({
    productId: {
        type:String,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    operation:{
        type: String,
        required: true
    }
});

const product = new mongoose.model("Product", productSchema);

module.exports = {
    product
}