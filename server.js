const http = require('http');
const productCollection = require('./model/product.model');
const mongoose = require('mongoose');
const connectionString = ''
// In the below statement provide the connection string.
// mongoose.connect(`${connectionString}`);

async function updateProduct(req, res){

    const allProducts = req.body; 
    try{
        allProducts.forEach(async (prod)=>{
            await modifyDB(prod);
        });
    }catch(e){
        // Handle error
    }
    
}

async function modifyDB(product){
    const {productId, quantity, operation} = product;

    // Below are async operations so use "await".
    if(operation == 'add'){
        // find the item by productId
        // update the quantity by adding
    }
    else if(operation == 'subtract'){
        // find the item by productId
        // update the quantity by subtracting
    }
}

const server = http.createServer(async (req, res)=>{
    const {url, method} =req;

    if(url == '/product' && method == 'POST'){
       await updateProduct(req, res);
    }
});

// Use bodyParser to enable utilization of JSON data received.



server.listen(8081,()=>{
    console.log('Server running 8081');
});